const express = require("express");
const app = express();
const cors = require("cors");
const mongoDb = require("mongodb");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const BankAccount = require("./models/bankDetail");

app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

mongoose
  .connect("mongodb://127.0.0.1/InTask")
  .then(() => {
    console.log("mongo db connected successfully");
  })
  .catch((err) => {
    console.log("mongoDb connected failed", err);
  });

// liston port

app.listen(5173, (err) => {
  if (!err) {
    console.log("server start 5173 port");
  }
});

// create Bank details
app.post("/MockBankAccount", async (req, res) => {
  try {
    const { name, account_number, balance } = req.body;

    console.log(JSON.stringify(account_number).length);

    if (JSON.stringify(account_number).length != 16) {
      return res.status(400).send({
        status: false,
        status_code: 400,
        message: "account number must 16 character",
      });
    }

    const bank_details = new BankAccount({
      name,
      account_number,
      balance,
    });
    await bank_details.save();

    return res.status(200).send({
      status: true,
      status_code: 200,
      message: "account created successfully",
    });
  } catch (error) {
    console.log(error);
    return res.status(400).send({
      status: false,
      status_code: 400,
      message: "Account create Failed",
      data: error,
    });
  }
});

// find account number

app.get("/balanceapi", async (req, res) => {
  try {
    const { account_number } = req.query;

    if (!account_number) {
      return res.status(400).send({
        status: false,
        status_code: 400,
        message: "account number required",
      });
    }

    const balance = await BankAccount.findOne({
      account_number,
    });
    if (!balance) {
      return res.status(200).send({
        status: false,
        status_code: 400,
        message: "No such Account Number",
      });
    }
    return res.status(200).send({
      status: true,
      status_code: 200,
      message: "Successfully fetched data",
      data: balance,
    });
  } catch (error) {
    return res.status(400).send({
      status: false,
      status_code: 400,
      message: "Balance List Failed",
      data: error,
    });
  }
});
