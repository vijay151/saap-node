const mongoose = require("mongoose");

const BankAccount = mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  account_number: {
    type: Number,
    require: true,
  },
  balance: {
    type: Number,
    require: true,
  },
});

module.exports = mongoose.model("Bank_account", BankAccount);
